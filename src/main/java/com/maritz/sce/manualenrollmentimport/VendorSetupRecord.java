package com.maritz.sce.manualenrollmentimport;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

@Data
public class VendorSetupRecord {
	
	@CsvBindByName(column = "VENDOR NUMBER")
	private String vendorNumber;

	@CsvBindByName(column = "VENDOR NAME LINE 1")
	private String vendorNameLine1;

	@CsvBindByName(column = "COMPANY_TYPE")
	private String companyType;
	
	@CsvBindByName(column = "VENDOR NAME LINE 2")
	private String vendorNameLine2;
	
	@CsvBindByName(column = "VENDOR NAME LINE 3")
	private String vendorNameLine3;
	
	@CsvBindByName(column = "VENDOR NAME LINE 4")
	private String vendorNameLine4;
	
	@CsvBindByName(column = "HOUSE NUMBER")
	private String houseNumber;
	
	@CsvBindByName(column = "STREET NAME")
	private String streetName;
	
	@CsvBindByName(column = "SUITE OR SUPPLIMENTAL ADDRESS NO.")
	private String suiteOrSupplementalAddressNo;
	
	@CsvBindByName(column = "CITY")
	private String city;
	
	@CsvBindByName(column = "STATE / REGION")
	private String stateRegion;
	
	@CsvBindByName(column = "CITY POSTAL CODE")
	private String cityPostalCode;
	
	@CsvBindByName(column = "COUNTRY")
	private String country;
	
	@CsvBindByName(column = "TAX NUMBER")
	private String taxNumber;
	
	@CsvBindByName(column = "Social Security #")
	private String ssn;
	
	@CsvBindByName(column = "VENDOR MAIN COMPANY TELEPHONE NUMBER")
	private String vendorMainCompanyTelephoneNumber;
	
	@CsvBindByName(column = "EMAIL ID")
	private String emailId;
	
	@CsvBindByName(column = "Routing Number")
	private String routingNumber;
	
	@CsvBindByName(column = "BANK ACCOUNT")
	private String bankAccount;
	
	@CsvBindByName(column = "ACCOUNT NAME")
	private String accountName;
	
	@CsvBindByName(column = "Reference Details 1")
	private String referenceDetails1;
	
	@CsvBindByName(column = "NAME OF BANK")
	private String nameOfBank;
	
	@CsvBindByName(column = "Country")
	private String country2;

}
