package com.maritz.sce.manualenrollmentimport;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class VendorMismatchFileWriter {
    
    private static final String[] MISMATCHED_FIELDS_COLUMN_ORDER = {"Vendor Number", "Mismatched Fields"};

    public void write(String filename, List<VendorMismatch> data) throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException {
        Writer writer = new FileWriter(filename);
        HeaderColumnNameMappingStrategy<VendorMismatch> mappingStrategy = new HeaderColumnNameMappingStrategy<>();
        mappingStrategy.setType(VendorMismatch.class);
        mappingStrategy.setColumnOrderOnWrite(new ColumnNameOrderComparator(MISMATCHED_FIELDS_COLUMN_ORDER));
        
        StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
                .withMappingStrategy(mappingStrategy)
                .build();
        beanToCsv.write(data);
        writer.close();
    }

}
