package com.maritz.sce.manualenrollmentimport;

public class VendorRecordMismatchCalculator {

    public VendorMismatch calculateMismatches(VendorResponseRecord r, VendorSetupRecord s) {
        VendorMismatch vendorMismatch = new VendorMismatch();
        vendorMismatch.setVendorNumber(r.getVendorNumber());
        
        String mismatchedFields = "";
        if (!r.getTaxNumber1().equals(s.getTaxNumber())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Tax Number 1");
        }
        
        if (!r.getTaxNumber2().equals(s.getSsn())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Tax Number 2");
        }
        
        if (!r.getVendorNameLine1().equals(s.getVendorNameLine1())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Vendor Name Line 1");
        }
        
        if (!r.getVendorNameLine2().equals(s.getVendorNameLine2())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Vendor Name Line 2");
        }

        if (!r.getHouseNumber().equals(s.getHouseNumber())) {
            mismatchedFields += appendFieldName(mismatchedFields, "House Number");
        }

        if (!r.getSupplemental().equals(s.getSuiteOrSupplementalAddressNo())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Supplemental");
        }

        if (!r.getCity().equals(s.getCity())) {
            mismatchedFields += appendFieldName(mismatchedFields, "City");
        }

        if (!r.getStateRegion().equals(s.getStateRegion())) {
            mismatchedFields += appendFieldName(mismatchedFields, "State/Region");
        }

        if (!r.getZip().equals(s.getCityPostalCode())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Zip");
        }

        if (!r.getEmail1().equals(s.getEmailId())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Email1");
        }

        if (!r.getCountry().equals(s.getCountry2())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Country");
        }

        if (!r.getBankKey().equals(s.getRoutingNumber())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Bank Key");
        }

        if (!r.getBankAccount().equals(s.getBankAccount())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Bank Account");
        }

        if (!r.getAccountHolder().equals(s.getAccountName())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Account Holder");
        }

        if (!r.getReferenceDetails().equals(s.getReferenceDetails1())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Reference Details");
        }

        if (!r.getNameOfBank().equals(s.getNameOfBank())) {
            mismatchedFields += appendFieldName(mismatchedFields, "Name of Bank");
        }

        vendorMismatch.setMismatchedFieldList(mismatchedFields);
        return vendorMismatch;
    }
    
    private String appendFieldName(String text, String fieldName) {
        return text.length() > 0 ? "::" + fieldName : fieldName;
    }
    


}
