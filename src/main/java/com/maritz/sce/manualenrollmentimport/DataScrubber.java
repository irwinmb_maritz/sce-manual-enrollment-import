package com.maritz.sce.manualenrollmentimport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataScrubber {
    
    private static final Logger log = LoggerFactory.getLogger(ManualEnrollmentImportApplication.class);

    public VendorResponseRecord scrubData(VendorResponseRecord v, VendorSetupRecord s) {
        if (isDroppedZeroPadding(s.getRoutingNumber(), v.getBankKey())) {
            log.info("Routing Number dropped padding");
            v.setBankKey(s.getRoutingNumber());
        }
        
        if (isDroppedZeroPadding(s.getBankAccount(), v.getBankAccount()) || isExponentialNotation(v.getBankAccount())) {
            log.info("Bank Acct Number dropped padding or exponential notation");
            v.setBankAccount(s.getBankAccount());
        }
        
        if (v.getBankKey().equals(s.getRoutingNumber()) && !v.getNameOfBank().equals(s.getNameOfBank())) {
            log.info("Fixing Bank Name");
            v.setNameOfBank(s.getNameOfBank());
        }
        
        if (v.getBankAccount().equals(s.getBankAccount()) && !v.getAccountHolder().equals(s.getAccountName())) {
            log.info("Fixing Account Name");
            v.setAccountHolder(s.getAccountName());
        }

        if (notSameButClose(s.getVendorNameLine1(), v.getVendorNameLine1())) {
            log.info("Fixing Vendor Name");
            v.setVendorNameLine1(s.getVendorNameLine1());
        }

        return v;
    }
    
    private boolean isDroppedZeroPadding(String sourceValue, String newValue) {
        return StringUtils.isNotBlank(newValue) && !newValue.equals(sourceValue) && StringUtils.leftPad(newValue, sourceValue.length(), "0").equals(sourceValue);
    }
    
    private boolean isExponentialNotation(String value) {
        return StringUtils.isNotBlank(value) && StringUtils.contains(value, "E+");
    }
    
    private boolean notSameButClose(String sourceValue, String newValue) {
        // Will update if trimmed value is same ignoring the casing
        return StringUtils.isNotBlank(newValue) 
                && !newValue.equals(sourceValue) && newValue.equalsIgnoreCase(StringUtils.strip(sourceValue));
    }

}
