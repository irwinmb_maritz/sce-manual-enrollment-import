package com.maritz.sce.manualenrollmentimport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.opencsv.bean.CsvToBeanBuilder;

@SpringBootApplication
public class ManualEnrollmentImportApplication {
    
    private static final Logger log = LoggerFactory.getLogger(ManualEnrollmentImportApplication.class);
    
    private static final String GRANT_TYPE = "client_credentials";
    
    @Value("${sce.api.client.id}")
    private String clientId;
    
    @Value("${sce.api.client.secret}")
    private String clientSecret;
    
    @Value("${sce.host}")
    private String host;
    
    private String authEndpoint = "/oauth2/tokens?grant_type={grantType}";
    private String downloadExtractEndpoint = "/batch/{batchId}/files?fileIdentifier=ExportFile.csv&inboundflag=false";

    private static final String DEFAULT_BATCH_NUMBER = "099";

    public static void main(String[] args) {
        SpringApplication.run(ManualEnrollmentImportApplication.class, args).close();
    }
    
    @Bean
    public CommandLineRunner run() throws Exception {
        return args -> {
            log.info("Starting scrubbing....");
            String batchNumber = DEFAULT_BATCH_NUMBER;
            if (args.length > 0) {
                log.info("Batch number ---> " + args[0]);
                batchNumber = args[0];
            }
            
            
            if (!fileExists(createVendorResponseFileName(batchNumber))) {
                return;
            }
            
            if (!fileExists(createVendorSetupFileName(batchNumber))) {
                log.info("Downloading enrollment extract data from API...");
                AuthToken authToken = getAuthToken();
                HttpEntity<String> request = buildAuthenticatedRequest(authToken);
                ResponseEntity<byte[]> downloadExtractData = downloadEnrollmentExtract(request, batchNumber);
                String encodedData = new String(downloadExtractData.getBody(), StandardCharsets.UTF_8);
                Files.write(Paths.get(createVendorSetupFileName(batchNumber)), Base64.getDecoder().decode(encodedData));
            }
            
            Map<String, VendorSetupRecord> vendorSetupRecordMap = loadVendorSetupDataToMap(batchNumber);

            List<VendorResponseRecord> vendorResponseRecords = loadVendorSetupToList(batchNumber);
            List<VendorMismatch> vendorMismatches = new ArrayList<>();
            
            DataScrubber dataScrubber = new DataScrubber();
            VendorRecordMismatchCalculator vendorRecordMismatchCalulator = new VendorRecordMismatchCalculator();
            
            for (VendorResponseRecord v : vendorResponseRecords) {
                log.info("Processing Vendor Number ==> " + v.getVendorNumber());
                VendorSetupRecord s = vendorSetupRecordMap.get(v.getVendorNumber());
                dataScrubber.scrubData(v, s);
                VendorMismatch vendorMismatch = vendorRecordMismatchCalulator.calculateMismatches(v, s);
                if (vendorMismatch != null && StringUtils.isNotBlank(vendorMismatch.getMismatchedFieldList())) {
                    vendorMismatches.add(vendorMismatch);
                }
            }
            
            if (!vendorMismatches.isEmpty()) {
                VendorMismatchFileWriter vendorMismatchFileWriter = new VendorMismatchFileWriter();
                vendorMismatchFileWriter.write(createVendorMismatchFileName(batchNumber), vendorMismatches);
            }
            
            CleanVendorResponseFileWriter cleanVendorResponseFileWriter = new CleanVendorResponseFileWriter();
            cleanVendorResponseFileWriter.write(createCleanVendorResponseFileName(batchNumber), vendorResponseRecords);
        };
    }

    private List<VendorResponseRecord> loadVendorSetupToList(String batchNumber) throws FileNotFoundException {
        List<VendorResponseRecord> vendorResponseRecords = new CsvToBeanBuilder(new FileReader(createVendorResponseFileName(batchNumber)))
                   .withType(VendorResponseRecord.class)
                   .build()
                   .parse();
        return vendorResponseRecords;
    }

    private Map<String, VendorSetupRecord> loadVendorSetupDataToMap(String batchNumber) throws FileNotFoundException {
        List<VendorSetupRecord> vendorSetupRecords = new CsvToBeanBuilder(new FileReader(createVendorSetupFileName(batchNumber)))
                .withType(VendorSetupRecord.class)
                .build()
                .parse();

        Map<String, VendorSetupRecord> vendorSetupRecordMap = vendorSetupRecords.stream()
                .collect(Collectors.toMap(VendorSetupRecord::getVendorNumber, vendorSetupRecord -> vendorSetupRecord));
        return vendorSetupRecordMap;
    }
    
    private boolean fileExists(String filename) {
        File f = new File(filename);
        return f.exists();
    }
    
    private String createFileName(String prefix, String batchNumber, String extension) {
        return prefix + batchNumber + extension;
    }
    
    private String createVendorSetupFileName(String batchNumber) {
        return createFileName("VendorSetup_", batchNumber, ".csv");
    }
    
    private String createVendorResponseFileName(String batchNumber) {
        return createFileName("VendorResponse-", batchNumber, ".csv");
    }
    
    private String createCleanVendorResponseFileName(String batchNumber) {
        return createFileName("VendorResponse-", batchNumber, ".clean.csv");
    }
    
    private String createVendorMismatchFileName(String batchNumber) {
        return createFileName("VendorMismatch-", batchNumber, ".csv");
    }
    
    private AuthToken getAuthToken() {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        
        HttpHeaders authHeaders = new HttpHeaders();
        authHeaders.setContentType(MediaType.APPLICATION_JSON);
        
        JSONObject authBody = new JSONObject();
        authBody.put("clientID", clientId);
        authBody.put("clientSecret", clientSecret);
        
        HttpEntity<String> authRequest = new HttpEntity<String>(authBody.toString(), authHeaders);
        
        ResponseEntity<AuthToken> respAuthToken = restTemplate.postForEntity(host + authEndpoint, authRequest, AuthToken.class, GRANT_TYPE);
        return respAuthToken.getBody();
    }
    
    private HttpEntity<String> buildAuthenticatedRequest(AuthToken authToken) {
        HttpHeaders sceHeaders = new HttpHeaders();
        sceHeaders.set("Authorization", "Bearer " + authToken.getAccessToken());        
        return new HttpEntity<String>(null, sceHeaders);
    }
    
    private ResponseEntity<byte[]> downloadEnrollmentExtract(HttpEntity<String>  request, String batchId) {
        Map<String, String> params = new HashMap<>();
        params.put("batchId", batchId);
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<byte[]> response = restTemplate.exchange(host + downloadExtractEndpoint, HttpMethod.GET, request, byte[].class, params);
        return response;
        
    }

}
