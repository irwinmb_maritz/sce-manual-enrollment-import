package com.maritz.sce.manualenrollmentimport;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

@Data
public class VendorResponseRecord {

    @CsvBindByName(column = "Vendor Number")
    private String vendorNumber;
    
    @CsvBindByName(column = "Vendor Account Group")
    private String vendorAccountGroup;
    
    @CsvBindByName(column = "Tax Number 1")
    private String taxNumber1;
    
    @CsvBindByName(column = "Tax Number 2")
    private String taxNumber2;
    
    @CsvBindByName(column = "Vendor Name Line 1")
    private String vendorNameLine1;
    
    @CsvBindByName(column = "Vendor Name Line 2")
    private String vendorNameLine2;
    
    @CsvBindByName(column = "House Number")
    private String houseNumber;
    
    @CsvBindByName(column = "Street")
    private String street;
    
    @CsvBindByName(column = "Supplemental")
    private String supplemental;
    
    @CsvBindByName(column = "City")
    private String city;
    
    @CsvBindByName(column = "State/Region")
    private String stateRegion;
    
    @CsvBindByName(column = "ZIP")
    private String zip;
    
    @CsvBindByName(column = "Email1")
    private String email1;
    
    @CsvBindByName(column = "Email2")
    private String email2;
    
    @CsvBindByName(column = "Email3")
    private String email3;
    
    @CsvBindByName(column = "Email4")
    private String email4;
    
    @CsvBindByName(column = "CTRY")
    private String country;
    
    @CsvBindByName(column = "Bank Key")
    private String bankKey;
    
    @CsvBindByName(column = "Bank Account")
    private String bankAccount;
    
    @CsvBindByName(column = "Acct Holder")
    private String accountHolder;
    
    @CsvBindByName(column = "Reference Details")
    private String referenceDetails;
    
    @CsvBindByName(column = "Name of Bank")
    private String nameOfBank;

}
