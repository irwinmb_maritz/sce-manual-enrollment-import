package com.maritz.sce.manualenrollmentimport;

import com.opencsv.bean.CsvBindByName;

import lombok.Data;

@Data
public class VendorMismatch {
    
    @CsvBindByName(column = "Vendor Number")
    private String vendorNumber;
    
    @CsvBindByName(column = "Mismatched Fields")
    private String mismatchedFieldList;

}
